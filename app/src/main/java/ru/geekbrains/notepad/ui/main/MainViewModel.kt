package ru.geekbrains.notepad.ui.main

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import ru.geekbrains.notepad.data.NoteRepository

class MainViewModel : ViewModel() {
    private val viewStateLiveData: MutableLiveData<MainViewState> = MutableLiveData()

    init {

        viewStateLiveData.value = MainViewState(NoteRepository.notes)
    }

    fun viewState(): LiveData<MainViewState> = viewStateLiveData
}