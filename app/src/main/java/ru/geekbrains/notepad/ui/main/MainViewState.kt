package ru.geekbrains.notepad.ui.main

import ru.geekbrains.notepad.data.entity.Note

class MainViewState(val notes: List<Note>)